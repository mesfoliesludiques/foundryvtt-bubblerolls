import { BUBBLEROLLS } from "../config.js";
CONFIG.bubblerolls = {};

Handlebars.registerHelper("truncate", function (inputstring, start, end) {
  var truncated = inputstring.substring(start, end);
  return new Handlebars.SafeString(truncated);
});

function _displayPool(part, keep) {
  let rollString = '';
  let color = '#FFFFFF'
  if (!keep) {
    color = "#AAAAAA";
  }
  part.results.forEach((roll, index) => {
    if (roll.result == part.faces && keep) {
      color = '#00A676';
    } else if (roll.result == 1 && index == 0) {
      color = '#C84630';
    }
    rollString += `<span class="roll" style="background-image: url('/modules/bubblerolls/assets/d${part.faces}.png');"><span style="color:${color};">${roll.result}</span></span>`; 
  });
  return rollString;
}

function _displayRoll(part) {
  let rollString = '';
  part.results.forEach((roll) => {
    rollString += `<span class="roll" style="background-image: url('/modules/bubblerolls/assets/d${part.faces}.png');"><span>${roll.result}</span></span>`; 
  });
  return rollString;
}

Handlebars.registerHelper("rollPart", function (part) {
  if (part instanceof Die) {
    return _displayRoll(part);
  }
  else if (part instanceof DicePool) {
    let poolString = '';
    part.dice.forEach(die => {
      if (part.modifiers == 'kh' && die.total == part.total) {
        poolString += _displayPool(die, true);
      } else {
        poolString += _displayPool(die, false);  
      }
    });
    return poolString;
  } else {
    return part;
  }
});

Hooks.once("init", async function () {
  // Localize template labels
  BUBBLEROLLS.templates = Object.entries(BUBBLEROLLS.templates).reduce(
    (obj, e) => {
      obj[e[0]] = game.i18n.localize(e[1]);
      return obj;
    },
    {}
  );

  game.settings.register("bubblerolls", "BubbleRollTemplate", {
    name: game.i18n.localize("BUBBLEROLLS.SettingsName"),
    hint: game.i18n.localize("BUBBLEROLLS.SettingsHint"),
    scope: "client",
    config: true,
    choices: BUBBLEROLLS.templates,
    default: "/modules/bubblerolls/templates/bubble-details.html",
    type: String,
    onChange: (value) => {
      CONFIG.bubblerolls.template = value;
    },
  });
  CONFIG.bubblerolls.template = game.settings.get(
    "bubblerolls",
    "BubbleRollTemplate"
  );
});

Hooks.on("renderChatMessage", async function (msg) {
  // Don't display old chat messages at refresh
  if (Date.now() - msg.data.timestamp > 500) {
    return;
  }
  if (msg.data.speaker.token && msg.isRoll && msg.isContentVisible) {
    const tok = canvas.tokens.placeables.find(
      (t) => (t.id == msg.data.speaker.token)
    );
    if (tok) {
      let doc = new DOMParser().parseFromString(msg.data.flavor, 'text/html');
      msg.data.flavor = doc.body.textContent;
      renderTemplate(CONFIG.bubblerolls.template, msg).then((html) => {
        canvas.hud.bubbles.say(tok, html, false);
      });
    }
  }
});
